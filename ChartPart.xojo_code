#tag Class
Protected Class ChartPart
Inherits Canvas
	#tag Event
		Function MouseDown(X As Integer, Y As Integer) As Boolean
		  dim d as dragItem
		  dim p as picture
		  // Handle Macindosh Drag and Drop
		  d = new DragItem(Self, me.left, me.top, me.width, me.height )
		  p = new Picture(me.width, me.height , screen(0).depth)
		  if not (p = nil) then
		    // handle low memory situations
		    drawToGraphics(p.graphics)
		    d.picture = p
		    d.drag
		  end if
		End Function
	#tag EndEvent

	#tag Event
		Sub Open()
		  System.DebugLog( me.Name + " :: ===== ChartPart Start =====" )
		  me.LoadPaletteValues()
		  me.resetChart
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub Paint(g As Graphics, areas() As REALbasic.Rect)
		  ' me.redraw()
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub addLegend(cLegendList as String)
		  // USAGE: Cp.addLegend("First","Second","Third",...)
		  // This is to display the value settings.
		  
		  me.cLegendValues = cLegendList
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub addSeries(cCaption AS String, cSeries AS String)
		  // USAGE: addSeries( "February", "12,15,1,5" )
		  DIM nValueCount AS Integer
		  nValueCount = CountFields( cSeries, "," )
		  self.valueCount = MAX( self.ValueCount, nValueCount )
		  
		  series.append (cSeries)
		  captions.append (cCaption)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub addValueLabel(nValue1 AS Double, nPointX AS Integer, nPointY AS Integer)
		  DIM oValueLabel AS NEW JSONItem
		  
		  // This method is intended to store the value labels for the various types of charts.  
		  // There are times where the drawing of a chart will wipe over, or partially wipe out
		  // a label.  It's beneficial to print the values after the chart itself is drawn to the screen.
		  // A good example is a pie chart with a thin slice of the pie.  Call self.drawValueLabels()
		  // to print the values that are stored in this method.
		  
		  oValueLabel.Value("Value") = nValue1
		  oValueLabel.Value("PointX") = nPointX
		  oValueLabel.Value("PointY") = nPointY
		  
		  self.valueLabels.Append( oValueLabel )
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub barChart(g as Graphics)
		  '] Method used to create a barChart on the canvas.  Normally called via self.drawToGraphics().
		  // To license chartPart, visit http://cullytechnologies.com/products/chartpart/readMe.html
		  
		  dim lnX, lnY, theMax, lnLeft, lnLeftStart, lnWidth, nCaptionCenter, nLabelY, nLegendLeft as integer = 0
		  dim barWidth, lnValue as double
		  dim lcSeries, lcValue AS String
		  dim oColor as Color
		  
		  // handle scaling
		  calcChartScale(g)
		  // draw the rules
		  drawRules(g)
		  
		  IF me.legendLocation = 2 THEN nLegendLeft = me.legendWidth
		  
		  // barWidth = {Width of chart / ( ( all bars of chart ) + (space between each series) + 1 )
		  barWidth = chartWidth / ( (ubound(series) * ValueCount) + ubound(series) + 1 )
		  
		  // Draw all of the shadow boxes
		  lnLeft = barWidth + marginLeft + nLegendLeft  // Start in one bar width
		  for lnX = 1 to ubound(series)
		    lcSeries = series(lnX)
		    for lnY = 1 to CountFields( lcSeries, "," )
		      lcValue = NthField( lcSeries, ",", lnY )
		      lnValue = VAL( lcValue )
		      me.barChartBar( g, lnLeft, lnValue, barWidth, shadowColor, True )
		      lnLeft = lnLeft + barWidth
		    next
		    lnLeft = lnLeft + barWidth  // Space between series.
		  next
		  
		  //Draw all of the color boxes
		  lnLeft = barWidth + marginLeft + nLegendLeft  // Start in one bar width
		  for lnX = 1 to ubound(series)
		    lnLeftStart = lnLeft
		    lcSeries = series(lnX)
		    for lnY = 1 to CountFields( lcSeries, "," )
		      lcValue = NthField( lcSeries, ",", lnY )
		      lnValue = VAL( lcValue )
		      oColor = getPaletteColor( lnY )
		      me.BarChartBar( g, lnLeft, lnValue, barWidth, oColor, False )
		      lnLeft = lnLeft + barWidth
		    next
		    
		    if labelSeries then
		      // Draw the series captions
		      nCaptionCenter = lnLeftStart + ( ( lnLeft - lnLeftStart ) / 2 )
		      g.ForeColor = me.titleColor
		      nLabelY = GetSeriesLabelLocation( g )
		      drawCenteredString( captions( lnX ), nCaptionCenter, nLabelY, g )
		    end if
		    
		    // Space between series
		    lnLeft = lnLeft + barWidth
		  next
		  
		  self.drawValueLabels( g )
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub barChartBar(g as graphics, nLeft as Integer, nValue as Double, nBarWidth as Integer, barColor as Color, lShadow as Boolean)
		  '] A private method that is used by self.barChart.  Used to create a single bar on the barChart.
		  DIM nTop, nLabelTop, nLabelLeft AS Integer= 0
		  DIM nHeight AS Integer = 1
		  DIM cLabel AS String = ""
		  
		  nHeight = chartHeight * ( abs(nValue) / (Max - Min) )
		  IF nValue < 0 THEN
		    // Negative value for our bar. Top is zero.
		    nTop = me.yZero + 1
		  ELSE
		    nTop = me.yZero - nHeight
		  END IF
		  
		  if lShadow then
		    nLeft = nLeft + shadowSize
		    nBarWidth = nBarWidth + me.shadowSize
		    if nValue < 0 then
		      nHeight = nHeight + me.shadowSize
		    else
		      nTop = nTop + me.shadowSize
		      nHeight = nHeight - me.shadowSize
		      if nHeight < 1 then nHeight = 0
		    end if
		  end if
		  
		  g.ForeColor = barColor
		  g.fillRect( nLeft, nTop, nBarWidth, nHeight )
		  
		  // Label values
		  if labelValues AND NOT lShadow then
		    'IF nValue = ROUND( nValue ) THEN
		    'cLabel = format( nValue, "-###" )
		    'ELSE
		    'cLabel = format( nValue, "-###.#" )
		    'END
		    nLabelLeft = nLeft + ( nBarWidth / 2 ) - ( g.StringWidth( cLabel ) / 2 )
		    nLabelTop = me.labelLocationTop( nValue, nTop, nTop + nHeight, g.TextHeight )
		    me.addValueLabel( nValue, nLabelLeft, nLabelTop )
		    
		    'if labelShadow then
		    'g.ForeColor = &cffffff
		    'g.DrawString( cLabel, nLabelLeft + 1, nLabelTop + 2, chartWidth )
		    'end if
		    'g.foreColor = labelColor
		    'g.drawString( cLabel, nLabelLeft, nLabelTop, chartWidth )
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub calcChartScale(g as Graphics)
		  dim nX, nY, nTotalMax, nTotalMin as integer
		  dim theMax, theMin, nBuffer as double = 0
		  dim nValue as double
		  dim cSeries, cValue as string
		  
		  chartWidth = g.Width - marginLeft - marginRight
		  chartHeight = g.Height - marginTop - marginBottom
		  // NOTE: Be sure that calcLegendScale is called before calcChartScale when showing a legend in your chart.
		  //         LegendWidth and LegendHeight need to be calcd before this call.
		  IF me.legendShow THEN
		    IF me.legendLocation = 3 OR me.legendLocation = 4 THEN
		      // legendLocation = Bottom or Top
		      chartHeight = chartHeight - legendHeight
		      IF chartHeight < 10 THEN chartHeight = 10    // Sanity check.
		    ELSE
		      // legendLocation = Right or Left
		      chartWidth = chartWidth - legendWidth
		      if chartWidth < 10 THEN chartWidth = 10       // Sanity check.
		    END
		  END
		  
		  if autoscale then
		    // do scaling based on data
		    for nX = 1 to ubound(series)
		      cSeries = Series( nX )
		      valueCount = CountFields( cSeries, "," )
		      if chartType = 4 then // stacked bar
		        nTotalMax = 0
		        nTotalMin = 0
		        for nY = 1 to valueCount
		          cValue = NthField( cSeries, ",", nY )
		          nValue = VAL( cValue )
		          IF nValue > 0 then
		            nTotalMax = nTotalMax + nValue
		          else
		            nTotalMin = nTotalMin + nValue
		          end if
		        next
		        theMax = MAX( theMax, nTotalMax )
		        theMin = MIN( theMin, nTotalMin )
		      else
		        for nY = 1 to valueCount
		          cValue = NthField( cSeries, ",", nY )
		          nValue = VAL( cValue )
		          theMax = MAX( theMax, nValue )
		          theMin = MIN( theMin, nValue )
		        next
		      end if
		    next
		    
		    // increase theMax by 10%
		    nBuffer = (theMax - theMin) * 0.10
		    theMax = theMax + nBuffer
		    if theMin < 0 then theMin = theMin - nBuffer
		    
		    IF theMax > 5 THEN
		      theMax = CEIL( theMax / 10 ) * 10
		      theMin = FLOOR( theMin / 10 ) * 10
		    ELSE
		      // increase theMax until we have integer step values...
		      theMax = CEIL( theMax )
		      theMin = FLOOR( theMin )
		    END
		    
		    //yScale = (chartHeight - (countFields(title, chr(13)) + 1) * g.textHeight -2) / (max - min)
		    yScale = (chartHeight  / (max - min))    // Now that we can account for a marginTop, we don't need to allow for the title. KJC 26/02/2014
		    min = theMin
		    max = theMax
		    if max = abs(min) then
		      // if we have a 20 max and a -20 min, then we're balanced and we want 5 steps
		      interval = (max - min) / 5
		    else
		      interval = (max - min) / 6
		    end if
		  else
		    // scale has been manually set up. Sanity check!
		    yscale = (chartHeight - (countFields(title, chr(13)) + 1) * g.textHeight -2) / (max - min)
		  end if
		  
		  if Max <> Min then
		    // Figure out where the yZero point is just to be handy for calculations.
		    yzero = (chartHeight * (max / (max - min) ) ) + marginTop
		    system.DebugLog( me.Name + " :: yZero: " + CSTR( yZero ) )
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub calcLegendScale()
		  DIM nLenWord, nMaxLen AS Double
		  DIM nX, nLegendCount AS Integer
		  DIM cWord AS String = ""
		  DIM p AS New Picture(me.Width,me.Height,32)
		  
		  IF me.autoScaleLegend THEN
		    p.Graphics.TextSize = me.legendTextSize
		    nLegendCount = CountFields( me.cLegendValues, "," )
		    IF nLegendCount > 0 THEN
		      FOR nX = 1 TO nLegendCount
		        cWord = NthField( me.cLegendValues, ",", nX )
		        nLenWord = p.Graphics.StringWidth( cWord )
		        nMaxLen = MAX( nMaxLen, nLenWord )
		      NEXT
		      me.legendTextHeight = p.Graphics.StringHeight( cWord, me.Width )
		      me.legendTextHeight = MAX( me.legendTextHeight, me.legendNodeSize )
		      me.legendWidth = me.legendPadding + me.legendNodeSize + me.legendLineSpacing + nMaxLen + me.legendPadding
		      me.legendHeight = me.legendPadding + (nLegendCount * ( me.legendTextHeight + me.legendLineSpacing ) ) + me.legendPadding
		      system.DebugLog( me.Name + " :: Legend Info: legendTextHeight=" + CSTR(me.LegendTextHeight) + "; " + _
		      "legendWidth=" + CSTR(me.legendWidth) + "; " + _
		      "legendHeight=" + CSTR(me.legendHeight) + "; " + _
		      "legendNodeSize=" + CSTR(me.legendNodeSize)  )
		    ELSE
		      system.DebugLog( me.Name + " :: there are not any legend values so not calculating legend size.")
		    END
		    
		  ELSE
		    system.DebugLog( me.Name + " :: autoScaleLegend is set to FALSE so not calculating legend size.")
		  END
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub clearChart()
		  TRY
		    IF me.graphics <> Nil THEN
		      me.graphics.ForeColor = me.bgColor
		      me.graphics.FillRect( 0, 0, me.Width, me.Height )
		    END IF
		  CATCH err AS NilObjectException
		    system.DebugLog( me.Name + " :: Problem in clearChart method. " + err.Message )
		  CATCH
		    BREAK
		  END
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub clearValues()
		  redim captions(0)
		  redim series(0)
		  cLegendValues = ""
		  me.legendWidth = 0
		  me.legendHeight = 0
		  me.valueLabels = NEW JSONItem
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub copyChart()
		  dim c as new clipboard
		  dim oPicture AS New Picture( me.Width, me.Height )
		  dim oGraphic as graphics
		  oGraphic = oPicture.graphics
		  me.drawToGraphics( oGraphic )
		  'oPicture.Graphics = oGraphic
		  c.Picture = oPicture
		  c.close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub drawCenteredString(str as String, x as Integer, y as Integer, g as Graphics)
		  dim newX, i as integer
		  dim tempStr as string
		  ' dim c as color
		  
		  ' c=g.foreColor
		  
		  // KJC 7/2017 - Commented out as we're going to have the calling method control the foreColor when called.
		  ' g.foreColor = titleColor
		  
		  for i = 1 to countFields(str, chr(13))
		    tempStr = nthField (str, chr(13), i)
		    newX = x - g.stringWidth(tempStr)/2
		    g.drawString(tempStr, newX, y + g.textHeight*(i-1), g.width)
		  next
		  ' g.forecolor = c
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub drawLegend()
		  drawLegend(me.Graphics)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub drawLegend(g as Graphics)
		  DIM nTop, nLeft, nX, nTopTemp, nNodeTop AS Integer = 0
		  DIM nLegendCount AS Integer = CountFields( me.cLegendValues, "," )
		  DIM cLegendEntry AS String = ""
		  DIM oColor AS Color
		  
		  g.foreColor = titleColor
		  g.TextSize = me.legendTextSize
		  g.penwidth = chartLineWidth
		  g.penHeight = chartLineWidth
		  
		  IF me.legendLocation = 2 THEN
		    // Left
		    nLeft = 1
		    nTop = ( me.chartHeight / 2 ) - ( me.legendHeight / 2 )
		  ELSE
		    // Right
		    nLeft = me.ChartWidth-1
		    nTop = ( me.ChartHeight / 2 ) - ( me.legendHeight / 2 )
		  END
		  
		  IF nLeft < 0 THEN nLeft = 0
		  IF nLeft > g.Width THEN nLeft = g.Width - 10    // Sanity Check.
		  IF nTop < 0 THEN nTop = 0
		  IF nTop > g.Height THEN nTop = g.Height - 10
		  
		  nTopTemp = nTop + me.legendPadding
		  FOR nX = 1 TO nLegendCount
		    cLegendEntry = NthField( me.cLegendValues, ",", nX )
		    oColor = getPaletteColor( nX )
		    
		    nNodeTop = nTopTemp + (me.legendTextHeight / 2) - (NodeSize / 2)
		    // Draw legend node shadow
		    g.ForeColor = shadowColor
		    g.fillRect( nLeft + me.legendPadding + shadowSize, nNodeTop + shadowSize, legendNodeSize, legendNodeSize )
		    
		    // Draw legend node
		    g.ForeColor = oColor
		    g.fillRect(nLeft + me.legendPadding, nNodeTop, legendNodeSize, legendNodeSize )
		    
		    // Draw legend entry string.
		    g.ForeColor = me.labelColor
		    g.drawString( cLegendEntry, nLeft + me.legendPadding + legendNodeSize + shadowSize + me.legendLineSpacing, nTopTemp+me.legendTextHeight, me.LegendWidth )
		    
		    nTopTemp = nTopTemp + me.legendLineSpacing + legendTextHeight
		  NEXT
		  
		  IF me.legendBorder THEN
		    g.ForeColor = me.legendBorderColor
		    g.drawRect(nLeft, nTop, legendWidth, legendHeight )
		  END IF
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub drawRules(g as Graphics)
		  dim nX as Double
		  dim ruleY, nGridStep, nMaxValue as Double
		  dim nRuleLeft, nLegendLeft AS Integer = 0
		  dim ruleWidth AS Integer = 3
		  dim cLabel as string
		  dim cFormat as String = "-###"
		  if rules>0  AND (Max <> 0  OR Min<>0) then
		    if (Max - Min) < 5 THEN cFormat = "-#.##"
		    
		    if me.legendShow and me.legendLocation = 2 THEN nLegendLeft = me.legendWidth
		    
		    if rules = 1 then
		      ruleWidth =  .03 * chartWidth
		    else
		      ruleWidth = chartWidth-1
		    end if
		    g.foreColor = titleColor
		    g.penwidth = chartLineWidth
		    g.penHeight = chartLineWidth
		    
		    nMaxValue = MAX( Max, ABS(Min) )
		    nGridStep = nMaxValue * 0.2
		    IF nGridStep > 10 THEN
		      DIM nGridStepNew AS Integer = ROUND( nGridStep )
		      DIM n10s AS Integer = LEN( STR( nGridStepNew ) ) - 1
		      DIM nRounder AS Integer = 10 ^ n10s
		      nGridStep = ROUND( nGridStep / nRounder ) * nRounder
		    END IF
		    
		    nRuleLeft = me.marginLeft + nLegendLeft
		    System.DebugLog( me.Name + " :: nGridStep=" + CSTR(nGridStep) + "; nRuleLeft=" + CSTR(nRuleLeft) ) + "; ruleWidth=" + CSTR(ruleWidth)
		    
		    // Positive values
		    nX = 0
		    DO UNTIL nX > Max
		      nX = nX + nGridStep
		      ruleY = me.yZero - ( chartHeight * ( nX / ( Max - Min) ) )
		      IF ruleY < marginTop THEN
		        EXIT
		      ELSE
		        g.drawLine( nRuleLeft, ruleY, nRuleLeft + ruleWidth - 1, ruleY)
		        cLabel = format( nX, cFormat )
		        g.drawString(cLabel, nRuleLeft + 1, ruleY - 1, chartWidth)
		      END
		    LOOP
		    
		    // Negative values
		    nX = 0
		    DO UNTIL nX < Min
		      nX = nX - nGridStep
		      ruleY = me.yZero + (chartHeight * ( ABS(nX) / (Max - Min) ) )
		      IF ruleY > g.Height - marginBottom THEN
		        EXIT
		      ELSE
		        g.drawLine( nRuleLeft, ruleY, nRuleLeft + ruleWidth - 1, ruleY)
		        cLabel = format( nX, cFormat )
		        g.drawString(cLabel, nRuleLeft + 1, ruleY - 1, chartWidth)
		      END
		    LOOP
		    
		    // Draw the zero line if charting negative numbers and zero is less than the bottom of the chart.
		    IF me.yzero < chartHeight Then
		      g.foreColor = &c000000
		      g.drawline( nRuleLeft, me.yzero, chartWidth - 1, me.yzero )
		      g.drawString( "0", nRuleLeft + 1, me.yzero - 1, chartWidth )
		    end if
		  end if
		  
		  // Test Vertical line - Comment out when in production
		  'g.DrawLine( nRuleLeft, 0, nRuleLeft, g.Height )
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub drawTitle(g as Graphics)
		  system.DebugLog( me.Name + " :: titleFont: " + g.TextFont + "; titleFontSize: " + cStr( g.TextSize ) )
		  // draw titles
		  //g.TextFont = titleFont
		  g.TextSize = titleTextSize
		  //g.ForeColor = titleColor
		  
		  drawCenteredString(title, g.width/2, g.textHeight + 2, g)
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub drawToGraphics(g as Graphics)
		  DIM cSeries AS String
		  
		  IF me.Palette = Nil or me.Palette.Count < 1 THEN
		    me.LoadPalette()
		  END
		  
		  IF UBound( Series) > 0 THEN
		    cSeries = Series( 1 )
		    IF CountFields( cSeries, "," ) > 0 THEN
		      me.ClearChart()
		      
		      me.calcLegendScale()
		      
		      g.TextSize = me.labelTextSize
		      
		      select case chartType
		      case 1
		        barChart(g)
		      case 2
		        pieChart(g)
		      case 3
		        lineChart(g)
		      case 4
		        stackedBarChart(g)
		      end
		      
		      me.showMargins()
		      me.drawTitle(g)
		      IF me.legendShow THEN
		        me.drawLegend(g)
		      END
		      
		      // now draw a frame
		      g.penwidth = 1
		      g.penHeight = 1
		      g.forecolor = rgB(0,0,0)
		      g.drawrect(0,0,g.width, g.Height)
		    ELSE
		      System.DebugLog( me.Name + " :: No values found in the first series. Not drawing chart." )
		    END IF
		  ELSE
		    System.DebugLog( me.Name + " :: No series found. Not drawing chart." )
		  END IF
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub drawValueLabels(g AS Graphics)
		  dim nValueLabelCount As Integer = valueLabels.Count
		  ' dim cValue as String = ""
		  dim nValue AS Double
		  dim oValueLabel AS JSONItem
		  dim nX, nPointX, nPointY AS Integer = 0
		  
		  // find total of values to be charted
		  for nX = 0 to nValueLabelCount-1
		    oValueLabel = valueLabels( nX )
		    
		    nValue = oValueLabel.Value("Value")
		    nPointX = oValueLabel.Value("PointX")
		    nPointY = oValueLabel.Value("PointY")
		    
		    // draw labels
		    DIM cLabelValue AS String = FORMAT( nValue, "-###,###,###,###.##" )
		    IF ROUND( nValue ) = nValue THEN
		      cLabelValue = FORMAT( nValue, "-###,###,###,###,###" )
		    END IF
		    ' drawCenteredString(cLabelValue, x + sin((nValue/2 + runningTotal) * totalRadians / total) * diameter/3, y + cos((nValue/2 + runningTotal) * totalRadians / total) * diameter/3 + g.textHeight/2, g)
		    
		    if labelShadow then
		      g.ForeColor = &cffffff
		      'IF charttype = 2 THEN // Pie
		      drawCenteredString( cLabelValue, nPointX+1, nPointY+1, g )
		      'ELSE
		      'g.DrawString( cLabelValue, nPointX + 1, nPointY + 1, chartWidth )
		      'END IF
		    END IF
		    g.foreColor = labelColor
		    
		    'IF chartType = 2 THEN // Pie
		    drawCenteredString( cLabelValue, nPointX, nPointY, g )
		    'ELSE
		    'g.DrawString( cLabelValue, nPointX, nPointY, chartWidth )
		    'END IF
		    
		    
		  NEXT
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getPaletteColor(nColor AS Integer) As Color
		  DIM nColorMod AS Integer
		  DIM oRetColor AS Color
		  
		  nColorMod = nColor MOD me.Palette.Count
		  IF nColorMod = 0 THEN nColorMod = me.Palette.Count
		  'system.DebugLog( me.Name + " :: getPaletteColor " + CSTR( nColor ) + " MOD " + CSTR(me.Palette.Count) + " = " + CSTR(nColorMod) )
		  oRetColor = me.Palette.Value( nColorMod )
		  
		  RETURN oRetColor
		  
		  
		  Exception err as NilObjectException
		    break
		    MsgBox "Tried to access a Nil object!"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getSeriesLabelLocation(g as Graphics) As Integer
		  DIM nRetVal AS Integer = 0
		  SELECT CASE me.labelSeriesLocation
		  CASE 1
		    nRetVal = marginTop + g.textHeight  // Top
		  CASE 2
		    nRetVal = me.yZero - g.TextHeight  // Above zero
		  CASE 3
		    nRetVal = me.yZero + g.TextHeight // Below zero
		  CASE 4
		    nRetVal = marginTop + chartHeight - g.textHeight // Bottom
		  ELSE
		    Break
		    nRetVal = 1 + g.textHeight
		  END
		  
		  IF nRetVal > marginTop + chartHeight then
		    nRetVal = marginTop + chartHeight
		  end if
		  
		  Return nRetVal
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function labelLocationTop(nValue AS Double, nTop AS Integer, nBottom AS Integer, nTextHeight AS Integer) As Integer
		  // me.labelValueLocation = 1 Top (Default); 2 = Bottom; 3 = Center Vertically
		  // Note, that the dimensions start in the upper left, so increasing numbers move down the canvas.
		  // Note: the text starts printing at the bottom of the letters so the nTextHeight is misleading on the calculation.
		  DIM nReturnLabelTop AS Integer = 0
		  
		  select case chartType
		  case 1, 4   // barChart(g)
		    
		    SELECT CASE labelValueLocation
		    CASE 1 // Top (Default)
		      if nValue > 0 then
		        nReturnLabelTop = nTop + nTextHeight + 0
		      else
		        nReturnLabelTop = nBottom - 2
		      end if
		    CASE 2 // Bottom
		      if nValue > 0 then
		        nReturnLabelTop = nBottom - 2
		      else
		        nReturnLabelTop = nTop + nTextHeight + 0
		      end if
		    CASE 3 // Centered
		      DIM nCenter AS Integer = ROUND( nBottom - ( ABS( nBottom - nTop ) / 2 ) )
		      nReturnLabelTop =  nCenter + ROUND( nTextHeight / 2) 
		    ELSE  // Top is the default
		      Break
		      // Shouldn't be here.
		    END SELECT
		    
		  case 2   // pieChart(g)
		    // Not yet.
		  case 3   // lineChart(g)
		    // Not yet.
		  end
		  
		  RETURN nReturnLabelTop
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub lineChart(g as Graphics)
		  // To license chartPart, visit http://cullytechnologies.com/products/chartpart/readMe.html
		  dim nSeriesCount, nValuesCount, nSeries, nValues as integer = 0
		  dim nFirstValue, nSecondValue as double
		  dim cFirstValue, cSecondValue as String = ""
		  dim lDrawThisLine as boolean = True
		  dim cSeries as string = ""
		  DIM oColor as Color
		  
		  nSeriesCount = UBound( series )
		  nValuesCount = countFields( series(1), "," )
		  
		  // handle scaling
		  calcChartScale(g)
		  // draw the rules
		  drawRules(g)
		  
		  g.penwidth = chartLineWidth
		  g.penHeight = chartLineWidth
		  
		  
		  
		  // draw the lines
		  for nValues = 1 to nValuesCount
		    cSeries = Series( 1 )
		    cFirstValue = NthField( cSeries, ",", nValues)
		    nFirstValue = Val( cFirstValue )
		    oColor = getPaletteColor( nValues )
		    for nSeries = 2 to nSeriesCount
		      cSeries = Series( nSeries )
		      cSecondValue = NthField( cSeries, ",", nValues )
		      nSecondValue = VAL( cSecondValue )
		      IF NOT me.ShowZeroValues AND ( nFirstValue = 0 OR nSecondValue = 0 ) THEN
		        ' Skip this line, to or from the point value.
		      ELSE
		        me.lineChartLine( g, nSeries, nFirstValue, nSecondValue, oColor )
		      END IF
		      nFirstValue = nSecondValue
		    next
		  next
		  
		  // draw the nodes
		  for nSeries = 1 to nSeriesCount
		    cSeries = Series( nSeries )
		    for nValues = 1 to nValuesCount
		      cFirstValue = NthField( cSeries, ",", nValues )
		      nFirstValue = VAL( cFirstValue )
		      IF NOT me.showZeroValues AND nFirstValue = 0 THEN
		        ' Skip printing this node 
		      ELSE
		        oColor = getPaletteColor( nValues )
		        me.lineChartNode( g, nSeries, nFirstValue, oColor )
		      END IF
		    next
		  next
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub lineChartLine(g as Graphics, nSeries as Integer, nFirstValue as Double, nSecondValue as Double, oColor as Color)
		  DIM nLeft1, nLeft2, nTop1, nTop2, nHeight as Double
		  DIM nLegendLeft AS Integer = 0
		  DIM nSeriesCount AS Integer = UBound( Series )
		  DIM nSeriesWidth AS Double = chartWidth / ( nSeriesCount + 1 )
		  
		  IF me.legendShow AND me.legendLocation=2 THEN nLegendLeft = me.legendWidth
		  nLeft2 = (nSeriesWidth * nSeries) + nLegendLeft
		  nLeft1 = (nSeriesWidth * (nSeries - 1) ) + nLegendLeft
		  
		  nHeight = chartHeight * ( abs(nSecondValue) / (Max - Min) )
		  IF nSecondValue < 0 THEN
		    nTop2 = me.yZero + nHeight
		  ELSE
		    nTop2 = me.yZero - nHeight
		  END IF
		  
		  nHeight = chartHeight * ( abs(nFirstValue) / (Max - Min) )
		  IF nFirstValue < 0 THEN
		    nTop1 = me.yZero + nHeight
		  ELSE
		    nTop1 = me.yZero - nHeight
		  END IF
		  
		  g.ForeColor = shadowColor
		  g.drawLine( nLeft1 + shadowSize, nTop1 + shadowSize, nLeft2 + shadowSize, nTop2 + shadowSize )
		  g.ForeColor = oColor
		  g.drawLine(nLeft1, nTop1, nLeft2, nTop2)
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub lineChartNode(g as Graphics, nSeries as Integer, nValue as Double, oColor as Color)
		  DIM nLeft, nTop, nHeight as Double
		  DIM cLabel, cSeries, cValue AS String = ""
		  DIM nSeriesCount AS Integer = UBound( Series )
		  DIM nSeriesWidth AS Double = chartWidth / ( nSeriesCount + 1 )
		  DIM nX, nLabelY, nLegendLeft AS Integer = 0
		  
		  IF me.legendShow AND me.legendLocation = 2 THEN nLegendLeft = me.legendWidth
		  nLeft = (nSeriesWidth * nSeries ) + nLegendLeft
		  nHeight = chartHeight * ( abs(nValue) / (Max - Min) )
		  IF nValue > 0 then
		    nTop = me.yZero - nHeight
		  else
		    nTop = me.yZero + nHeight
		  end if
		  
		  // Offset the node so the center is the data point
		  nLeft = nLeft - (NodeSize / 2)
		  nTop = nTop - (NodeSize / 2)
		  
		  g.ForeColor = shadowColor
		  g.fillRect(nLeft+ shadowSize, nTop + shadowSize, NodeSize, NodeSize )
		  
		  g.ForeColor = oColor
		  g.fillRect(nLeft, nTop, NodeSize, NodeSize )
		  
		  // label Values in the chart
		  if labelValues then
		    IF nValue = ROUND( nValue ) THEN
		      cLabel = format( nValue, "-###" )
		    ELSE
		      cLabel = format( nValue, "-###.#" )
		    END
		    if nTop - g.TextHeight < 0 then nTop = nTop + NodeSize + g.TextHeight  // Handle if label is off top of chart
		    if labelShadow then
		      g.ForeColor = &cffffff
		      g.DrawString( cLabel, nLeft + 1, nTop + 1, chartWidth)  // This should make the labels 'pop'.
		    end if
		    g.foreColor = labelColor
		    g.drawString( cLabel, nLeft, nTop, chartWidth )
		  end if
		  
		  if labelSeries then
		    cLabel = captions( nSeries )
		    g.ForeColor = titleColor
		    nLabelY = me.GetSeriesLabelLocation( g )
		    me.drawCenteredString( cLabel, nLeft, nLabelY, g )
		  end if
		  
		  if seriesValues then
		    // Draw the series values as labels in the chart
		    cSeries = Series( nSeries )
		    g.ForeColor = me.titleColor
		    me.lineChartSeriesValue( g, cSeries, nLeft )
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub lineChartSeriesValue(g as Graphics, cSeries as string, nLeft as Integer)
		  DIM cValue, cLabel AS String = ""
		  DIM nValue, nTotal, nMin, nMax AS Double = 0
		  DIM nX, nLabelY AS Integer = 0
		  
		  FOR nX = 1 TO CountFields( cSeries, "," )
		    cValue = NthField( cSeries, ",", nX )
		    nValue = Val( cValue )
		    IF nValue < 0 then
		      nMin = MIN( nMin, nValue )
		    ELSE
		      nMax = MAX( nMax, nValue )
		    END IF
		    nTotal = nTotal + nValue
		  NEXT
		  
		  SELECT CASE me.seriesValueLocation
		  CASE 1
		    nLabelY = g.textHeight + 2
		  CASE 2
		    nLabelY = me.yZero - ( chartHeight * ( ABS(nMax) / (Max - Min) ) )
		  CASE 3
		    nLabelY = me.yZero + ( chartHeight * ( ABS(nMin) / (Max - Min) ) ) + g.TextHeight
		  CASE 4
		    nLabelY = chartHeight
		  END
		  
		  cLabel = CSTR( nTotal )
		  DrawCenteredString( cLabel, nLeft + 1, nLabelY, g )
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadPalette()
		  SELECT CASE me.nPalette
		  CASE 1
		    me.Palette = me.PaletteBerry
		  CASE 2
		    me.Palette = me.PaletteBright
		  CASE 3
		    me.Palette = me.PaletteBrightPastel
		  CASE 4
		    me.Palette = me.PaletteChocolate
		  CASE 5
		    me.Palette = me.PaletteEarthTones
		  CASE 6
		    me.Palette = me.PaletteGrayScale
		  ELSE
		    me.Palette = me.PaletteBerry
		  END SELECT
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadPaletteValues()
		  me.PaletteList = NEW Dictionary
		  me.PaletteList.Value(1) = "Berry"
		  me.PaletteList.Value(2) = "Bright"
		  me.PaletteList.Value(3) = "Bright Pastel"
		  me.PaletteList.Value(4) = "Chocolate"
		  me.PaletteList.Value(5) = "Earth Tones"
		  me.PaletteList.Value(6) = "Gray Scale"
		  
		  
		  //Berry
		  me.PaletteBerry = NEW Dictionary
		  me.PaletteBerry.Value(1) = RGB(138,43,226)
		  me.PaletteBerry.Value(2) = RGB(186,85,211)
		  me.PaletteBerry.Value(3) = RGB(65,105,225)
		  me.PaletteBerry.Value(4) = RGB(199,21,133)
		  me.PaletteBerry.Value(5) = RGB(0,0,255)
		  me.PaletteBerry.Value(6) = RGB(138,43,226)
		  me.PaletteBerry.Value(7) = RGB(218,112,214)
		  me.PaletteBerry.Value(8) = RGB(123,104,238)
		  me.PaletteBerry.Value(9) = RGB(192,0,192)
		  me.PaletteBerry.Value(10) = RGB(0,0,205)
		  me.PaletteBerry.Value(11) = RGB(128,0,128)
		  
		  //Bright
		  me.PaletteBright = NEW Dictionary
		  me.PaletteBright.Value(1) = RGB(0,128,0)
		  me.PaletteBright.Value(2) = RGB(0,0,255)
		  me.PaletteBright.Value(3) = RGB(128,0,128)
		  me.PaletteBright.Value(4) = RGB(0,255,0)
		  me.PaletteBright.Value(5) = RGB(255,0,255)
		  me.PaletteBright.Value(6) = RGB(0,128,128)
		  me.PaletteBright.Value(7) = RGB(255,255,0)
		  me.PaletteBright.Value(8) = RGB(128,128,128)
		  me.PaletteBright.Value(9) = RGB(0,255,255)
		  me.PaletteBright.Value(10) = RGB(0,0,128)
		  me.PaletteBright.Value(11) = RGB(128,0,0)
		  me.PaletteBright.Value(12) = RGB(255,0,0)
		  me.PaletteBright.Value(13) = RGB(128,128,0)
		  me.PaletteBright.Value(14) = RGB(192,192,192)
		  me.PaletteBright.Value(15) = RGB(255,99,71)
		  me.PaletteBright.Value(16) = RGB(255,228,181)
		  
		  //Bright Pastel
		  me.PaletteBrightPastel = NEW Dictionary
		  me.PaletteBrightPastel.Value(1) = RGB(65,140,240)
		  me.PaletteBrightPastel.Value(2) = RGB(252,180,65)
		  me.PaletteBrightPastel.Value(3) = RGB(224,64,10)
		  me.PaletteBrightPastel.Value(4) = RGB(5,100,146)
		  me.PaletteBrightPastel.Value(5) = RGB(191,191,191)
		  me.PaletteBrightPastel.Value(6) = RGB(26,59,105)
		  me.PaletteBrightPastel.Value(7) = RGB(255,227,130)
		  me.PaletteBrightPastel.Value(8) = RGB(18,156,221)
		  me.PaletteBrightPastel.Value(9) = RGB(202,107,75)
		  me.PaletteBrightPastel.Value(10) = RGB(0,92,219)
		  me.PaletteBrightPastel.Value(11) = RGB(243,210,136)
		  me.PaletteBrightPastel.Value(12) = RGB(80,99,129)
		  me.PaletteBrightPastel.Value(13) = RGB(241,185,168)
		  me.PaletteBrightPastel.Value(14) = RGB(224,131,10)
		  me.PaletteBrightPastel.Value(15) = RGB(120,147,190)
		  
		  //Chocolate
		  me.PaletteChocolate = NEW Dictionary
		  me.PaletteChocolate.Value(1) = RGB(160,82,45)
		  me.PaletteChocolate.Value(2) = RGB(210,105,30)
		  me.PaletteChocolate.Value(3) = RGB(139,0,0)
		  me.PaletteChocolate.Value(4) = RGB(205,133,63)
		  me.PaletteChocolate.Value(5) = RGB(165,42,42)
		  me.PaletteChocolate.Value(6) = RGB(244,164,96)
		  me.PaletteChocolate.Value(7) = RGB(139,69,19)
		  me.PaletteChocolate.Value(8) = RGB(192,64,0)
		  me.PaletteChocolate.Value(9) = RGB(178,34,34)
		  me.PaletteChocolate.Value(10) = RGB(182,92,58)
		  
		  //Earth Tones
		  me.PaletteEarthTones = NEW Dictionary
		  me.PaletteEarthTones.Value(1) = RGB(255,128,0)
		  me.PaletteEarthTones.Value(2) = RGB(184,134,11)
		  me.PaletteEarthTones.Value(3) = RGB(192,64,0)
		  me.PaletteEarthTones.Value(4) = RGB(107,142,35)
		  me.PaletteEarthTones.Value(5) = RGB(205,133,63)
		  me.PaletteEarthTones.Value(6) = RGB(192,192,0)
		  me.PaletteEarthTones.Value(7) = RGB(34,139,34)
		  me.PaletteEarthTones.Value(8) = RGB(210,105,30)
		  me.PaletteEarthTones.Value(9) = RGB(128,128,0)
		  me.PaletteEarthTones.Value(10) = RGB(30,177,169)
		  me.PaletteEarthTones.Value(11) = RGB(244,164,96)
		  me.PaletteEarthTones.Value(12) = RGB(0,192,0)
		  me.PaletteEarthTones.Value(13) = RGB(143,188,139)
		  me.PaletteEarthTones.Value(14) = RGB(178,34,34)
		  me.PaletteEarthTones.Value(15) = RGB(139,69,19)
		  me.PaletteEarthTones.Value(16) = RGB(192,0,0)
		  
		  //Gray Scale
		  me.PaletteGrayScale = NEW Dictionary
		  me.PaletteGrayScale.Value(1) = RGB(200,200,200)
		  me.PaletteGrayScale.Value(2) = RGB(189,189,189)
		  me.PaletteGrayScale.Value(3) = RGB(178,178,178)
		  me.PaletteGrayScale.Value(4) = RGB(167,167,167)
		  me.PaletteGrayScale.Value(5) = RGB(156,156,156)
		  me.PaletteGrayScale.Value(6) = RGB(145,145,145)
		  me.PaletteGrayScale.Value(7) = RGB(134,134,134)
		  me.PaletteGrayScale.Value(8) = RGB(123,123,123)
		  me.PaletteGrayScale.Value(9) = RGB(112,112,112)
		  me.PaletteGrayScale.Value(10) = RGB(101,101,101)
		  me.PaletteGrayScale.Value(11) = RGB(90,90,90)
		  me.PaletteGrayScale.Value(12) = RGB(79,79,79)
		  me.PaletteGrayScale.Value(13) = RGB(68,68,68)
		  me.PaletteGrayScale.Value(14) = RGB(57,57,57)
		  me.PaletteGrayScale.Value(15) = RGB(46,46,46)
		  me.PaletteGrayScale.Value(16) = RGB(35,35,35)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub pieChart(g as Graphics)
		  // To license chartPart, visit http://cullytechnologies.com/products/chartpart/readMe.html
		  
		  // NOTE: We only graph one series when working with pie charts.
		  
		  DIM nLegendLeft, nLegendRight, nLegendTop, nLegendBottom AS Integer = 0
		  IF me.legendShow THEN
		    SELECT CASE me.legendLocation
		    CASE 1
		      nLegendRight = me.legendWidth
		    CASE 2
		      nLegendLeft = me.legendWidth
		    CASE 3
		      nLegendBottom = me.legendHeight
		    CASE 4
		      nLegendTop = me.legendHeight
		    END
		  END
		  
		  me.chartWidth = me.Width - me.marginLeft - me.marginRight - nLegendRight - nLegendLeft
		  me.chartHeight = me.Height - me.marginTop - me.marginBottom - nLegendBottom - nLegendTop
		  
		  dim nX, nJ, points(0), topMargin, total, runningTotal, x, y, diameter as integer
		  dim pi As Double = 3.1415926
		  dim totalRadians as double = pi * 2 // the number of radians in a circle
		  dim cSeries AS String = Series(1)
		  dim nChartValCount As Integer = CountFields( cSeries, "," )
		  dim cValue as String
		  dim nValue, nValueMax AS Double
		  
		  // find total of values to be charted
		  for nX = 1 to nChartValCount
		    cValue = NthField( cSeries, ",", nX )
		    nValue = VAL( cValue )
		    nValueMax = MAX( nValueMax, nValue )
		    total = total + nValue
		  next
		  '
		  // figure a few values
		  topMargin = (countFields(title, chr(13)) + 1) * me.graphics.textHeight -2
		  x = (chartWidth/2) + marginLeft + nLegendLeft
		  y = marginTop + (chartHeight / 2 ) + nLegendTop
		  
		  // figure the circle's diameter/
		  if chartWidth - 20 < chartHeight - 20 then
		    diameter = chartWidth - 20
		  else
		    diameter = chartHeight - 20
		  end if
		  ' dim segmentConstant As Integer = 50 // The lower this value, the more segments in each curve. *!* Commented out by KJC on 7/2017
		  dim segmentConstant As Integer = nValueMax  // New methodology for calculating this. Higher values killed our performance. This corrects that problem.
		  
		  if shadowSize > 0 then
		    // draw the circle and shadow
		    g.foreColor = shadowColor
		    g.fillOval(x-diameter/2 + shadowSize, y-diameter/2 + shadowSize, diameter, diameter)
		  end
		  
		  if not useColoredValues then
		    g.foreColor = chartColor
		    g.fillOval(x-diameter/2, y-diameter/2, diameter, diameter)
		  end if
		  // now plot each of the lines
		  g.penwidth = chartLineWidth
		  g.penHeight = chartLineWidth
		  g.foreColor = titleColor
		  runningTotal = 0
		  for nX = 1 to nChartValCount
		    cValue = NthField( cSeries, ",", nX)
		    nValue = VAL( cValue )
		    if useColoredValues then
		      // draw slices
		      redim points(0)
		      points.Append x
		      points.Append y
		      DIM nStart AS Integer = runningTotal * (diameter / segmentConstant)
		      DIM nEnd AS Integer = (runningTotal + nValue) * (diameter / segmentConstant)
		      DIM nPrevX, nPrevY AS Integer = -1
		      For nJ = nStart To nEnd
		        DIM nNewX AS Integer = x + (sin((nJ/ (diameter / segmentConstant)) * totalRadians / total) * diameter/2)
		        DIM nNewY AS Integer = y + (cos((nJ / (diameter / segmentConstant)) *totalRadians / total) * diameter/2)
		        IF nNewX = nPrevX AND nNewY = nPrevY THEN
		          // Do nothing
		        ELSE
		          // New point so add it to the array of points!
		          Points.Append nNewX
		          Points.Append nNewY
		          ' System.DebugLog( "Value=" + cValue + ";nJ=" + STR(nJ) + " of " + STR(nStart) + " to " + STR( nEnd ) + "; nNewX=" + STR( nNewX ) + "; nNewY=" + STR( nNewY) )
		          nPrevX = nNewX
		          nPrevY = nNewY
		        END IF
		      Next
		      
		      if nX = nChartValCount then
		        // make sure the last and first slices match up
		        Points.Append x + (sin(0) * diameter/2)
		        Points.Append y + (cos(0) * diameter/2)
		      else
		        Points.Append x + (sin((runningTotal + nValue) * totalRadians / total) * diameter/2)
		        Points.Append y + (cos((runningTotal + nValue) *totalRadians / total) * diameter/2)
		      end if
		      g.ForeColor = getPaletteColor( nX )
		      g.FillPolygon Points
		    else
		      // draw lines between slices¡
		      g.drawLine(x,y, x + sin((nValue + runningTotal) * totalRadians / total) * diameter/2, y + cos((nValue + runningTotal) * totalRadians / total) * diameter/2)
		    end if
		    // draw labels
		    'DIM cLabelValue AS String = FORMAT( nValue, "###,###,###,###.##" )
		    'IF ROUND( nValue ) = nValue THEN
		    'cLabelValue = FORMAT( nValue, "###,###,###,###,###" )
		    'END IF
		    'drawCenteredString(cLabelValue, x + sin((nValue/2 + runningTotal) * totalRadians / total) * diameter/3, y + cos((nValue/2 + runningTotal) * totalRadians / total) * diameter/3 + g.textHeight/2, g)
		    DIM nPointX AS Double = x + sin((nValue/2 + runningTotal) * totalRadians / total) * diameter/3
		    DIM nPointY AS Double = y + cos((nValue/2 + runningTotal) * totalRadians / total) * diameter/3 + g.textHeight/2
		    self.addValueLabel( nValue, nPointX, nPointY )   // Save this information for printing of the Value Labels last.
		    
		    runningTotal = runningTotal + nValue
		  next
		  
		  self.drawValueLabels( g )
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub printChart()
		  dim g as graphics
		  g = openPrinterDialog(pageSetup)
		  if not (g = nil) then
		    drawToGraphics(g)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub redraw()
		  if me.visible then
		    drawToGraphics(me.graphics)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub resetChart()
		  title=""
		  autoscale = true
		  bgcolor = rgb(255,255,255)
		  chartColor = rgb(128,128,128)
		  shadowColor = rgb(50,50,50)
		  titleColor = rgb(0,0,0)
		  shadowSize = 1
		  rules = 1
		  chartLineWidth = 2
		  interval = 5
		  valueLabels = NEW JSONItem   // The valueLabels that get printed last when printing charts.
		  'chartType = 1
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub saveChart(cChartFileName AS String)
		  Dim oFolder as FolderItem
		  DIM oDate AS New Date
		  Dim oPicture As New Picture( me.Width, me.Height, 32)
		  
		  me.DrawToGraphics( oPicture.Graphics )
		  
		  If Picture.IsExportFormatSupported(Picture.FormatPNG) Then
		    oFolder = SpecialFolder.Documents.Child( cChartFileName )
		    
		    // Save the image out to the file
		    TRY
		      oPicture.Save(oFolder, Picture.SaveAsPNG)
		    CATCH err AS NilObjectException
		      msgBox("Cannot save: " + err.message)
		    CATCH err AS IOException
		      msgBox( "Cannot save for IO Error: " + err.message )
		    END TRY
		  ELSE
		    System.DebugLog( me.Name + " :: Cannot export as a PNG" )
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub showMargins()
		  IF me.ShowMargins THEN
		    DIM nWidth, nHeight, nLegendLeft, nLegendRight as Integer = 0
		    
		    IF me.legendShow THEN
		      IF me.legendLocation = 1 THEN nLegendRight = me.legendWidth
		      IF me.legendLocation = 2 THEN nLegendLeft = me.legendWidth
		    END
		    
		    me.Graphics.foreColor = titleColor
		    me.Graphics.penwidth = chartLineWidth
		    me.Graphics.penHeight = chartLineWidth
		    nWidth = me.Width - me.marginLeft - me.marginRight - me.legendWidth
		    nHeight = me.Height - me.marginTop - me.marginBottom
		    
		    me.Graphics.drawRect( me.marginLeft + nLegendLeft, me.marginTop, nWidth, nHeight )
		  ELSE
		    system.DebugLog( me.Name + " :: ShowMargins is set to FALSE so not drawing margins.")
		  END
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub stackedBarChart(g as Graphics)
		  // To license chartPart, visit http://cullytechnologies.com/products/chartpart/readMe.html
		  
		  dim nX, nY, theMax, nLeft, lnLeftStart, lnWidth, nCaptionCenter, nRunningMax, nRunningMin, nRetVal, nLabelY, nLegendLeft as integer = 0
		  dim barWidth, nValue as double
		  dim cSeries, cValue AS String
		  dim oColor as Color
		  
		  // handle scaling
		  calcChartScale(g)
		  // draw the rules
		  drawRules(g)
		  
		  // barWidth = {Width of chart / ( ( all series of chart ) + (space between each series) + 1 )
		  barWidth = chartWidth / ( ( ubound(series) * 2 )  +  1 )
		  
		  IF me.legendLocation = 2 THEN nLegendLeft = me.legendWidth
		  nLeft = barWidth + marginLeft + nlegendLeft  // Start in one bar width
		  for nX = 1 to ubound(series)
		    system.DebugLog( me.Name + " :: >>> Series: " + CSTR(nX) + " of " + CSTR(ubound(series)) )
		    cSeries = series(nX)
		    nRunningMax = me.yZero + 1
		    nRunningMin = me.yZero + 1
		    for nY = 1 to CountFields( cSeries, "," )
		      cValue = NthField( cSeries, ",", nY )
		      nValue = VAL( cValue )
		      oColor = getPaletteColor(nY)
		      nRetVal = me.stackedBarChartBar( g, nLeft, nValue, nRunningMax, nRunningMin, barWidth, oColor )
		      if nValue < 0 then
		        nRunningMin = nRunningMin + nRetVal
		      else
		        nRunningMax = nRunningMax - nRetVal
		      end if
		      ' nLeft = nLeft + 2 // uncomment for debugging purposes.  It staggers the bars.
		    next
		    nLeft = nLeft + ( barWidth * 2)  // Space between series.
		  next
		  
		  
		  if labelSeries then
		    // Draw the series captions
		    nLeft = barWidth + (barWidth / 2) + nLegendLeft
		    nLabelY = me.GetSeriesLabelLocation( g )
		    g.ForeColor = me.titleColor
		    for nX = 1 TO ubound(captions)
		      drawCenteredString( captions( nX ), nLeft, nLabelY, g )
		      nLeft = nLeft + (barWidth * 2)
		    next
		  end if
		  
		  if seriesValues then
		    // Draw the series values as labels in the chart
		    nLeft = barWidth + (barWidth / 2) + nLegendLeft
		    g.ForeColor = me.titleColor
		    for nX = 1 to ubound(series)
		      cSeries = Series( nX )
		      me.stackedBarChartSeriesValue( g, cSeries, nLeft )
		      nLeft = nLeft + (barWidth * 2)
		    next
		  end if
		  
		  drawValueLabels( g )  // Print the labels above the stacked bars.
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function stackedBarChartBar(g as graphics, nLeft as Integer, nValue as Integer, nRunningMax as Integer, nRunningMin as Integer, nBarWidth as Integer, barColor as Color) As Integer
		  DIM nTop, nLabelTop, nLabelLeft AS Integer= 0
		  DIM nShadowTop, nShadowLeft, nShadowHeight AS Integer = 0
		  DIM nHeight AS Integer = 1
		  DIM cLabel, cLabelMessage AS String = ""
		  
		  nHeight = chartHeight * ( abs(nValue) / (Max - Min) )
		  IF nValue < 0 THEN
		    // Negative value for our bar. Top is zero.
		    nTop = nRunningMin + 1
		  ELSE
		    nTop = nRunningMax - nHeight -1
		  END IF
		  
		  if me.ShadowSize > 0 then
		    g.ForeColor = shadowColor
		    nShadowLeft = nLeft + nBarWidth
		    nShadowHeight = nHeight
		    // Side shadow
		    if nValue > 0 then
		      nShadowTop = nTop + me.shadowSize
		      if nShadowTop + nShadowHeight > me.yZero then
		        nShadowHeight = me.yZero - nShadowTop
		        if nShadowHeight < 0 then
		          nShadowHeight = 0
		        end if
		      end if
		    else
		      nShadowTop = nTop
		    end if
		    if nShadowHeight > 0 then
		      g.fillRect( nShadowLeft, nShadowTop, me.ShadowSize, nShadowHeight )
		    end if
		    // Bottom shadow
		    if nValue < 0 then
		      nShadowTop = nTop + nHeight
		      nShadowLeft = nLeft + me.shadowSize
		      g.fillRect( nShadowLeft, nShadowTop, nBarWidth, me.ShadowSize )
		    end if
		  end if
		  
		  g.ForeColor = barColor
		  g.fillRect( nLeft, nTop, nBarWidth, nHeight )
		  
		  // Label values
		  if labelValues then
		    DIM lContinue AS Boolean = ( nValue <> 0 ) OR ShowZeroValues
		    IF lContinue THEN 
		      'IF nValue = ROUND( nValue ) THEN
		      'cLabel = format( nValue, "-###" )
		      'ELSE
		      'cLabel = format( nValue, "-###.#" )
		      'END
		      nLabelLeft = nLeft + ( nBarWidth / 2 ) - ( g.StringWidth( cLabel ) / 2 )
		      nLabelTop = me.labelLocationTop( nValue, nTop, nTop + nHeight, g.TextHeight )
		      'if labelShadow then
		      'g.ForeColor = &cffffff
		      'g.DrawString( cLabel, nLabelLeft + 1, nLabelTop + 1, chartWidth )
		      'end if
		      'g.foreColor = labelColor
		      'g.drawString( cLabel, nLabelLeft, nLabelTop, chartWidth )
		      addValueLabel( nValue, nLabelLeft, nLabelTop )
		      cLabelMessage = " : LTop: " + CSTR(nLabelTop) + " w LHt: " + CSTR(g.TextHeight)
		    ELSE
		      cLabelMessage = ": Zero value so not showing."
		    end if
		  end if
		  
		  system.DebugLog( me.Name + " :: Stacked Bar value: " + CSTR(nValue) + " (" + CSTR( nTop) + " to " + CSTR( nTop + nHeight) + ")" + cLabelMessage )
		  
		  return nHeight
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub stackedBarChartSeriesValue(g as Graphics, cSeries as string, nLeft as Integer)
		  DIM cValue, cLabel AS String = ""
		  DIM nValue, nTotal, nMin, nMax AS Double = 0
		  DIM nX, nLabelY AS Integer = 0
		  
		  FOR nX = 1 TO CountFields( cSeries, "," )
		    cValue = NthField( cSeries, ",", nX )
		    nValue = Val( cValue )
		    IF nValue < 0 then
		      nMin = nMin + nValue
		    ELSE
		      nMax = nMax + nValue
		    END IF
		    nTotal = nTotal + nValue
		  NEXT
		  
		  SELECT CASE me.seriesValueLocation
		  CASE 1
		    nLabelY = g.textHeight + 2
		  CASE 2
		    nLabelY = me.yZero - ( chartHeight * ( ABS(nMax) / (Max - Min) ) )
		  CASE 3
		    nLabelY = me.yZero + ( chartHeight * ( ABS(nMin) / (Max - Min) ) ) + g.TextHeight
		  CASE 4
		    nLabelY = chartHeight
		  END
		  
		  cLabel = CSTR( nTotal )
		  DrawCenteredString( cLabel, nLeft + 1, nLabelY, g )
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub TestPattern()
		  DIM nTop, nLeft, nLeft2 as Integer = 0
		  DIM oRandom AS NEW Random
		  
		  me.Graphics.foreColor = titleColor
		  me.Graphics.penwidth = chartLineWidth
		  me.Graphics.penHeight = chartLineWidth
		  for nTop = 10 to me.height Step 40
		    nLeft = nTop
		    nLeft2 = me.Width - nTop
		    me.Graphics.drawLine(nLeft, nTop, nLeft2, nTop)
		  next
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		#tag Note
			'] Set to 'False' if you want to force a manually set scale to your charts, as opposed to having the class determine the chart scale.
		#tag EndNote
		autoScale As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			'] Set to 'false' if you would like manually set the size of the legend in your chart.
			'] Otherwise the legend size will automatically be scaled based on the number of values set via self.addLegend.
		#tag EndNote
		autoScaleLegend As Boolean = true
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			
			'] PURPOSE: If set to 'False' it allows the developer to set their own value in nGridSteps
		#tag EndNote
		autoScaleRules As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		bgcolor As color
	#tag EndProperty

	#tag Property, Flags = &h0
		captions(0) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		chartColor As color
	#tag EndProperty

	#tag Property, Flags = &h0
		chartHeight As Integer = 100
	#tag EndProperty

	#tag Property, Flags = &h0
		chartLineWidth As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			1 = Bar
			2 = Pie
			3 = Line
			4 = StackedBar
		#tag EndNote
		chartType As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		chartWidth As Integer = 100
	#tag EndProperty

	#tag Property, Flags = &h0
		cLegendValues As String
	#tag EndProperty

	#tag Property, Flags = &h0
		interval As Double = 1.0
	#tag EndProperty

	#tag Property, Flags = &h0
		labelColor As Color = &c333333
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			// NOTE: Set to TRUE to have the series values displayed in the chart.
		#tag EndNote
		labelSeries As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			// USAGE: 1=Top; 2 = Above zero; 3 = Below zero; 4 = Bottom
			// NOTE: Set labelSeries = False to hide labels.
		#tag EndNote
		labelSeriesLocation As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		labelShadow As Boolean = true
	#tag EndProperty

	#tag Property, Flags = &h0
		labelTextSize As Integer = 12
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			1 = Top (Default)
			2 = Bottom
			3 = Center
			
			// Note: This property initially affects only bar and stacked bar charts.
			// Doesn't apply to Pie or Line charts. KJC 6/23/2017
		#tag EndNote
		labelValueLocation As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		labelValues As boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		legendBorder As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		legendBorderColor As Color = &c000000
	#tag EndProperty

	#tag Property, Flags = &h0
		legendHeight As Integer = 20
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected legendLineHeight As Integer = 10
	#tag EndProperty

	#tag Property, Flags = &h0
		legendLineSpacing As Integer = 2
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			// 1 = right; 2 = left; 3 = bottom; 4 = top
		#tag EndNote
		legendLocation As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		legendNodeSize As Integer = 4
	#tag EndProperty

	#tag Property, Flags = &h0
		legendPadding As Integer = 10
	#tag EndProperty

	#tag Property, Flags = &h0
		legendShow As boolean = true
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected legendTextHeight As Integer = 10
	#tag EndProperty

	#tag Property, Flags = &h0
		legendTextSize As Integer = 12
	#tag EndProperty

	#tag Property, Flags = &h0
		legendWidth As Integer = 20
	#tag EndProperty

	#tag Property, Flags = &h0
		lineColor As Color = &c000000
	#tag EndProperty

	#tag Property, Flags = &h0
		marginBottom As Integer = 2
	#tag EndProperty

	#tag Property, Flags = &h0
		marginLeft As Integer = 2
	#tag EndProperty

	#tag Property, Flags = &h0
		marginRight As Integer = 2
	#tag EndProperty

	#tag Property, Flags = &h0
		marginTop As Integer = 25
	#tag EndProperty

	#tag Property, Flags = &h0
		max As Double = 99999
	#tag EndProperty

	#tag Property, Flags = &h0
		min As Double = 100
	#tag EndProperty

	#tag Property, Flags = &h0
		nodeSize As Integer = 4
	#tag EndProperty

	#tag Property, Flags = &h0
		nPalette As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		pageSetup As printerSetup
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected Palette As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected PaletteBerry As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected PaletteBright As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected PaletteBrightPastel As dictionary
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected PaletteChocolate As dictionary
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected PaletteEarthTones As dictionary
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected PaletteGrayScale As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		PaletteList As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		rules As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		series(0) As string
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			// USAGE: 1 = top of chart; 2 = above maximum; 3 = below minimum; 4 = bottom of chart.
		#tag EndNote
		seriesValueLocation As Integer = 2
	#tag EndProperty

	#tag Property, Flags = &h0
		#tag Note
			// USAGE: Set to TRUE to display the series values totals.
		#tag EndNote
		seriesValues As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		shadowColor As color = &cdddddd
	#tag EndProperty

	#tag Property, Flags = &h0
		shadowSize As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		showMargins As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		showZeroValues As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		title As String = "ChartPart Title"
	#tag EndProperty

	#tag Property, Flags = &h0
		titleColor As color
	#tag EndProperty

	#tag Property, Flags = &h0
		titleFont As String = "Helvetica"
	#tag EndProperty

	#tag Property, Flags = &h0
		titleTextSize As Integer = 16
	#tag EndProperty

	#tag Property, Flags = &h0
		useColoredValues As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		valueCount As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		valueLabels As JSONItem
	#tag EndProperty

	#tag Property, Flags = &h0
		yscale As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		yzero As Integer = 0
	#tag EndProperty


	#tag Constant, Name = Version, Type = Double, Dynamic = False, Default = \"5.4", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptFocus"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AcceptTabs"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoDeactivate"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="autoScale"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="autoScaleLegend"
			Group="Behavior"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="autoScaleRules"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Backdrop"
			Visible=true
			Group="Appearance"
			Type="Picture"
			EditorType="Picture"
		#tag EndViewProperty
		#tag ViewProperty
			Name="bgcolor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="chartColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="chartHeight"
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="chartLineWidth"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="chartType"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="chartWidth"
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="cLegendValues"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DoubleBuffer"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="EraseBackground"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Appearance"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
			EditorType="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="InitialParent"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="interval"
			Group="Behavior"
			InitialValue="1.0"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelColor"
			Group="Behavior"
			InitialValue="&c333333"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelSeries"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelSeriesLocation"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelShadow"
			Group="Behavior"
			InitialValue="true"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelTextSize"
			Group="Behavior"
			InitialValue="12"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelValueLocation"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="labelValues"
			Group="Behavior"
			InitialValue="True"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="legendBorder"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="legendBorderColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="legendHeight"
			Group="Behavior"
			InitialValue="20"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="legendLineSpacing"
			Group="Behavior"
			InitialValue="2"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="legendLocation"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="legendNodeSize"
			Group="Behavior"
			InitialValue="4"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="legendPadding"
			Group="Behavior"
			InitialValue="2"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="legendShow"
			Group="Behavior"
			InitialValue="false"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="legendTextSize"
			Group="Behavior"
			InitialValue="12"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="legendWidth"
			Group="Behavior"
			InitialValue="20"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lineColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="marginBottom"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="marginLeft"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="marginRight"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="marginTop"
			Group="Behavior"
			InitialValue="10"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="max"
			Group="Behavior"
			InitialValue="99999"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="min"
			Group="Behavior"
			InitialValue="100"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nodeSize"
			Group="Behavior"
			InitialValue="4"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nPalette"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rules"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="seriesValueLocation"
			Group="Behavior"
			InitialValue="2"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="seriesValues"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="shadowColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="shadowSize"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="showMargins"
			Group="Behavior"
			InitialValue="false"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="showZeroValues"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
			EditorType="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabIndex"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabPanelIndex"
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabStop"
			Visible=true
			Group="Position"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="title"
			Group="Behavior"
			InitialValue="ChartPart Title"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="titleColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="titleFont"
			Group="Behavior"
			InitialValue="System"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="titleTextSize"
			Group="Behavior"
			InitialValue="16"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Transparent"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
			EditorType="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="useColoredValues"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseFocusRing"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="valueCount"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="yscale"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="yzero"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

#tag Window
Begin Window Window1
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   628
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   533610495
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "ChartPart - v"
   Visible         =   True
   Width           =   768
   Begin ChartPart CP
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      autoScale       =   False
      autoScaleLegend =   True
      autoScaleRules  =   True
      Backdrop        =   0
      bgcolor         =   &c00000000
      chartColor      =   &c00000000
      chartHeight     =   100
      chartLineWidth  =   1
      chartType       =   1
      chartWidth      =   100
      cLegendValues   =   ""
      DoubleBuffer    =   False
      Enabled         =   True
      EraseBackground =   True
      Height          =   315
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      interval        =   1.0
      labelColor      =   &c33333300
      labelSeries     =   True
      labelSeriesLocation=   1
      labelShadow     =   True
      labelTextSize   =   12
      labelValueLocation=   1
      labelValues     =   True
      Left            =   20
      legendBorder    =   True
      legendBorderColor=   &c00000000
      legendHeight    =   20
      legendLineSpacing=   2
      legendLocation  =   1
      legendNodeSize  =   4
      legendPadding   =   2
      legendShow      =   False
      legendTextSize  =   12
      legendWidth     =   20
      lineColor       =   &c00000000
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      marginBottom    =   0
      marginLeft      =   0
      marginRight     =   0
      marginTop       =   10
      max             =   99999.0
      min             =   100.0
      nodeSize        =   4
      nPalette        =   1
      rules           =   0
      Scope           =   0
      seriesValueLocation=   2
      seriesValues    =   False
      shadowColor     =   &c00000000
      shadowSize      =   1
      showMargins     =   False
      showZeroValues  =   True
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      title           =   "ChartPart Title"
      titleColor      =   &c00000000
      titleFont       =   "System"
      titleTextSize   =   16
      Top             =   21
      Transparent     =   True
      useColoredValues=   True
      UseFocusRing    =   True
      valueCount      =   0
      Visible         =   True
      Width           =   728
      yscale          =   0.0
      yzero           =   0
   End
   Begin TabPanel TabPanel1
      AutoDeactivate  =   True
      Bold            =   False
      Enabled         =   True
      Height          =   270
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   False
      Panels          =   ""
      Scope           =   0
      SmallTabs       =   False
      TabDefinition   =   "Chart Examples\rSeries&Values\rAppearance\rFonts\rLabels\rMargins\rLegend"
      TabIndex        =   25
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   338
      Underline       =   False
      Value           =   0
      Visible         =   True
      Width           =   728
      Begin Label lblSeries
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   109
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   0
         TabPanelIndex   =   2
         Text            =   "Series Count:"
         TextAlign       =   0
         TextColor       =   &c33333300
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   408
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   266
      End
      Begin Slider sldSeriesCount
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   26
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   109
         LineStep        =   1
         LiveScroll      =   False
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Maximum         =   26
         Minimum         =   2
         PageStep        =   20
         Scope           =   2
         TabIndex        =   1
         TabPanelIndex   =   2
         TabStop         =   True
         TickStyle       =   "0"
         Top             =   429
         Value           =   5
         Visible         =   True
         Width           =   266
      End
      Begin Label lblPalette
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   40
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   0
         TabPanelIndex   =   3
         Text            =   "Color Palette"
         TextAlign       =   0
         TextColor       =   &c33333300
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   427
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin PopupMenu cboPalette
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   28
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         InitialValue    =   ""
         Italic          =   False
         Left            =   40
         ListIndex       =   0
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   3
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   451
         Underline       =   False
         Visible         =   True
         Width           =   191
      End
      Begin Label lblLineChartNodeSize
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   265
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   2
         TabPanelIndex   =   3
         Text            =   "Line Chart :: nodeSize:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   481
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   191
      End
      Begin Slider sldNodeSize
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   26
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   265
         LineStep        =   2
         LiveScroll      =   True
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Maximum         =   20
         Minimum         =   2
         PageStep        =   20
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   3
         TabStop         =   True
         TickStyle       =   "0"
         Top             =   502
         Value           =   4
         Visible         =   True
         Width           =   191
      End
      Begin CheckBox chkRules
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Chart Rules"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   265
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Scope           =   0
         State           =   0
         TabIndex        =   4
         TabPanelIndex   =   3
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   553
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   201
      End
      Begin Label lblShadow
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   265
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   5
         TabPanelIndex   =   3
         Text            =   "shadowSize:"
         TextAlign       =   0
         TextColor       =   &c33333300
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   427
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Slider sldShadow
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   26
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   265
         LineStep        =   1
         LiveScroll      =   False
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Maximum         =   10
         Minimum         =   0
         PageStep        =   20
         Scope           =   0
         TabIndex        =   6
         TabPanelIndex   =   3
         TabStop         =   True
         TickStyle       =   "1"
         Top             =   446
         Value           =   2
         Visible         =   True
         Width           =   191
      End
      Begin ScrollBar scrSeriesValueLocation
         AcceptFocus     =   True
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   17
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   353
         LineStep        =   1
         LiveScroll      =   False
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Maximum         =   4
         Minimum         =   1
         PageStep        =   20
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   5
         TabStop         =   True
         Top             =   511
         Value           =   1
         Visible         =   True
         Width           =   200
      End
      Begin CheckBox chkSeriesValues
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Series Values"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   353
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Scope           =   0
         State           =   0
         TabIndex        =   1
         TabPanelIndex   =   5
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   405
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   123
      End
      Begin CheckBox chkLabelSeries
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Label Series"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   76
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Scope           =   0
         State           =   1
         TabIndex        =   3
         TabPanelIndex   =   5
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   4
         Top             =   405
         Underline       =   False
         Value           =   True
         Visible         =   True
         Width           =   123
      End
      Begin CheckBox chkLabelShadow
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Label Shadow"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   76
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Scope           =   0
         State           =   1
         TabIndex        =   4
         TabPanelIndex   =   5
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   457
         Underline       =   False
         Value           =   True
         Visible         =   True
         Width           =   123
      End
      Begin CheckBox chkLabelValues
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Label Values"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   76
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Scope           =   0
         State           =   1
         TabIndex        =   5
         TabPanelIndex   =   5
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   4
         Top             =   431
         Underline       =   False
         Value           =   True
         Visible         =   True
         Width           =   123
      End
      Begin CheckBox chkShowLegend
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "legendShow"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   1
         TabIndex        =   0
         TabPanelIndex   =   7
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   395
         Underline       =   False
         Value           =   True
         Visible         =   True
         Width           =   237
      End
      Begin Label lblLabelTextSize
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   143
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   1
         TabPanelIndex   =   4
         Text            =   "labelTextSize:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   460
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   241
      End
      Begin UpDownArrows spnLabelFontSize
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   118
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   4
         TabStop         =   True
         Top             =   460
         Visible         =   True
         Width           =   13
      End
      Begin UpDownArrows spnTitleFontSize
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   118
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   4
         TabStop         =   True
         Top             =   409
         Visible         =   True
         Width           =   13
      End
      Begin Label lblMarginTop
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   88
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   1
         TabPanelIndex   =   6
         Text            =   "marginTop:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   399
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   236
      End
      Begin Label lblMarginLeft
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   439
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   3
         TabPanelIndex   =   6
         Text            =   "marginLeft:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   402
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   185
      End
      Begin Label lblMarginBottom
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   86
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   4
         TabPanelIndex   =   6
         Text            =   "marginBottom:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   427
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   201
      End
      Begin Label lblMarginRight
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   439
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   5
         TabPanelIndex   =   6
         Text            =   "marginRight:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   427
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   211
      End
      Begin Label lblChartWidth
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   69
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   6
         TabPanelIndex   =   6
         Text            =   "chartWidth:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   460
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   193
      End
      Begin UpDownArrows spnMarginTop
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   69
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   8
         TabPanelIndex   =   6
         TabStop         =   True
         Top             =   399
         Visible         =   True
         Width           =   14
      End
      Begin UpDownArrows spnMarginLeft
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   420
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   9
         TabPanelIndex   =   6
         TabStop         =   True
         Top             =   399
         Visible         =   True
         Width           =   14
      End
      Begin UpDownArrows spnMarginBottom
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   69
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   10
         TabPanelIndex   =   6
         TabStop         =   True
         Top             =   427
         Visible         =   True
         Width           =   14
      End
      Begin UpDownArrows spnMarginRight
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   420
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   11
         TabPanelIndex   =   6
         TabStop         =   True
         Top             =   427
         Visible         =   True
         Width           =   14
      End
      Begin Label lblChartHeight
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   69
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   7
         TabPanelIndex   =   6
         Text            =   "chartHeight:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   1
         Top             =   485
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   204
      End
      Begin CheckBox chkShowMargins
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "showMargins"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   69
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   12
         TabPanelIndex   =   6
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   528
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   130
      End
      Begin ScrollBar ScrSeriesLabelLocation
         AcceptFocus     =   True
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   17
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   355
         LineStep        =   1
         LiveScroll      =   False
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Maximum         =   4
         Minimum         =   1
         PageStep        =   20
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   5
         TabStop         =   True
         Top             =   465
         Value           =   1
         Visible         =   True
         Width           =   200
      End
      Begin Label lblTitleTextSize
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   143
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   0
         TabPanelIndex   =   4
         Text            =   "titleTextSize:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   409
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   241
      End
      Begin Label lblValues
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   109
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   2
         TabPanelIndex   =   2
         Text            =   "Value Count:"
         TextAlign       =   0
         TextColor       =   &c33333300
         TextFont        =   "System"
         TextSize        =   14.0
         TextUnit        =   0
         Top             =   481
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   259
      End
      Begin Slider sldValueCount
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   26
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   109
         LineStep        =   1
         LiveScroll      =   False
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Maximum         =   26
         Minimum         =   2
         PageStep        =   20
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   2
         TabStop         =   True
         TickStyle       =   "0"
         Top             =   502
         Value           =   5
         Visible         =   True
         Width           =   266
      End
      Begin Label lblLegendNodeSize
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   106
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   2
         TabPanelIndex   =   7
         Text            =   "legendNodeSize:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   429
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   207
      End
      Begin UpDownArrows spnLegendNodeSize
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   81
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   7
         TabStop         =   True
         Top             =   429
         Visible         =   True
         Width           =   13
      End
      Begin Label lblLegendPadding
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   106
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   4
         TabPanelIndex   =   7
         Text            =   "legendPadding:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   461
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   207
      End
      Begin UpDownArrows spnLegendPadding
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   81
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   5
         TabPanelIndex   =   7
         TabStop         =   True
         Top             =   461
         Visible         =   True
         Width           =   13
      End
      Begin Label lblLegendLineSpacing
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   106
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   6
         TabPanelIndex   =   7
         Text            =   "legendLineSpacing:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   496
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   207
      End
      Begin UpDownArrows spnLegendLineSpacing
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   81
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   7
         TabPanelIndex   =   7
         TabStop         =   True
         Top             =   496
         Visible         =   True
         Width           =   13
      End
      Begin Label lblLegendTextSize
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   106
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   8
         TabPanelIndex   =   7
         Text            =   "legendTextSize:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   528
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   207
      End
      Begin UpDownArrows spnLegendTextSize
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   81
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   9
         TabPanelIndex   =   7
         TabStop         =   True
         Top             =   528
         Visible         =   True
         Width           =   13
      End
      Begin Label lblLegendLocation
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   368
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   10
         TabPanelIndex   =   7
         Text            =   "legendLocation:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   429
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   284
      End
      Begin UpDownArrows spnLegendLocation
         AcceptFocus     =   False
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   23
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   343
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   11
         TabPanelIndex   =   7
         TabStop         =   True
         Top             =   429
         Visible         =   True
         Width           =   13
      End
      Begin Label lblSeriesLabelLocation
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   353
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   7
         TabPanelIndex   =   5
         Text            =   "Series Label Loc:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   439
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   202
      End
      Begin ChartExamples2 ChartExamples21
         AcceptFocus     =   False
         AcceptTabs      =   True
         AutoDeactivate  =   True
         BackColor       =   &cFFFFFF00
         Backdrop        =   0
         Enabled         =   True
         EraseBackground =   True
         HasBackColor    =   False
         Height          =   190
         HelpTag         =   ""
         InitialParent   =   "TabPanel1"
         Left            =   40
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   1
         TabStop         =   True
         Top             =   387
         Transparent     =   True
         UseFocusRing    =   False
         Visible         =   True
         Width           =   688
      End
      Begin CheckBox chkLegendBorder
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "legendBorder"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   343
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   1
         TabIndex        =   12
         TabPanelIndex   =   7
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   495
         Underline       =   False
         Value           =   True
         Visible         =   True
         Width           =   137
      End
      Begin Label lbllabelValueLocation
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   76
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   8
         TabPanelIndex   =   5
         Text            =   "labelValueLocation:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   491
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   209
      End
      Begin Slider sldLabelValueLocation
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   24
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Left            =   76
         LineStep        =   1
         LiveScroll      =   False
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Maximum         =   3
         Minimum         =   1
         PageStep        =   20
         Scope           =   0
         TabIndex        =   9
         TabPanelIndex   =   5
         TabStop         =   True
         TickStyle       =   "0"
         Top             =   511
         Value           =   1
         Visible         =   True
         Width           =   146
      End
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  self.Top = 50
		  self.Left = 50
		  self.Height = self.Height + 10
		  
		  self.Title = self.Title + CSTR( CP.Version )
		  CP.marginTop = 25
		  
		  ' system.log( system.LogLevelWarning, "Start application.")
		  LoadColorPalettes()
		  self.ShowMarginValues()
		  self.showLegendValues()
		  self.showFontInfo()
		  CP.nPalette= 5
		  self.ExampleBar1()
		  
		  
		  
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub ChartCopy()
		  CP.copyChart
		  MsgBox "Your chart should be in the Clipboard now.  Try pasting it into another document."
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ChartPrint()
		  CP.printChart()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ChartSave()
		  DIM oDate AS New Date
		  DIM cChartFileName AS String = "ChartPart_" + oDate.SQLDate + ".png"
		  CP.saveChart( cChartFileName )
		  MsgBox( "Your chart should now be saved in the DOCUMENTS folder: " + cChartFileName )
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExampleBar1()
		  Self.nValueMax = 100
		  Self.nValueMin = -100
		  Self.GenRandomData()
		  CP.legendShow = True
		  CP.charttype = 1
		  CP.redraw
		  Self.showLegendValues()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExampleBar2()
		  // This graph is different in that we're forcing values and the key color
		  // to achieve the special graph.
		  CP.resetChart
		  CP.clearValues
		  CP.chartType = 1
		  CP.nPalette = 6    // Grayscale
		  
		  CP.addSeries( "January", "1,2,3" )
		  CP.addSeries( "February" , "4,5,6" )
		  CP.addSeries( "March", "7,8,9" )
		  CP.addSeries( "April", "0,9,8" )
		  CP.addSeries( "May", "7,6,5" )
		  CP.addSeries( "June", "4,3,2" )
		  CP.addSeries( "August", "1,0,1" )
		  CP.addSeries( "September", "2,3,4" )
		  CP.addSeries( "October", "5,6,7" )
		  CP.addSeries( "November", "8,9,0" )
		  CP.addSeries( "December", "9,8,7" )
		  
		  CP.title = "Sales by Month"
		  CP.titleColor = rgb(0,128,0)
		  CP.redraw
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExampleBar3()
		  CP.resetChart
		  CP.clearValues
		  CP.chartType = 1
		  CP.nPalette = 6    // Grayscale
		  
		  CP.addSeries( "Hendrix", "0.1,-0.2,0.7" )
		  CP.addSeries( "Page" , "0.4,-0.5,0.6" )
		  CP.addSeries( "Slash", "0.3,0.8,-0.2" )
		  CP.addSeries( "Buckethead", "0.7,0.3,0.8" )
		  CP.addSeries( "Hammett", "-0.7,0.2,0.5" )
		  
		  CP.addLegend("Made,Up,Data" )
		  
		  CP.title = "Best Hard Rock Guitarists"
		  CP.titleColor = rgb(0,128,0)
		  CP.redraw
		  
		  CP.legendShow = True
		  CP.charttype = 1
		  CP.redraw
		  self.showLegendValues()
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExampleBar4()
		  CP.resetChart
		  CP.clearValues
		  CP.chartType = 1
		  CP.nPalette = 6    // Grayscale
		  Self.nValueMax = 1000000
		  Self.nValueMin = -100000
		  Self.GenRandomData()
		  CP.legendShow = True
		  CP.redraw
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExampleLegendOnly()
		  CP.clearValues()
		  CP.clearChart()
		  CP.legendLocation = 2   // 1=right; 2=left; (TBD: 3=top; 4=bottom)
		  CP.AddLegend("Apples,Bananas,Carrots,Donuts")
		  CP.AutoScaleLegend = True
		  CP.calcLegendScale()
		  CP.drawLegend()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExampleLine1()
		  self.GenRandomData()
		  CP.charttype = 3
		  
		  CP.redraw
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExamplePie1()
		  DIM oRandom AS New Random
		  self.GenRandomData2()
		  
		  CP.charttype = 2
		  CP.shadowSize = self.sldShadow.Value
		  
		  CP.redraw
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExampleStackedBar()
		  self.GenRandomData()
		  
		  CP.charttype = 4
		  CP.shadowSize = self.sldShadow.Value
		  
		  CP.redraw
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GenRandomData()
		  DIM oRandom AS New Random
		  DIM nX, nY, nValueInt AS Integer
		  DIM nValueDbl AS Double
		  // Look up these words!  It's a hoot.
		  DIM cWords AS String = "aeolist, bombilate, causeuse, dompteuse, estrapade, farctate, gargalesis, hadeharia, inaniloquent, jentacular, knismesis, liripip, mytacism, nelipot, onychophagy, petrichor, qualtagh, sabrage, thelemic, ulotrichous, ventripotent, witzelsucht, xerophagy, yclept, zenzizenzizenzic"
		  DIM cLegendWords AS String = "one,two,three,four,five,size,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen,twenty,twentyone,twentytwo,twentythree,twentyfour,twentyfive,twentysix"
		  DIM cSeries AS String = ""
		  DIM cCaption, cLegend AS String = ""
		  CP.resetChart
		  CP.clearValues
		  
		  'if self.nValueMax < 6 THEN
		  'break
		  'end
		  
		  FOR nX = 1 TO self.sldSeriesCount.Value
		    cSeries = ""
		    FOR nY = 1 TO self.sldValueCount.Value
		      IF self.nValueMax > 5 THEN
		        nValueInt = oRandom.InRange( nValueMin, nValueMax )
		        cSeries = cSeries + Str( nValueInt )
		      ELSE
		        nValueDbl = ( oRandom.InRange( nValueMin*10, nValueMax*10 ) / oRandom.InRange( 1, 10 ) )
		        nValueDbl = Round( nValueDbl * 10 ) / 10
		        cSeries = cSeries + Str( nValueDbl )
		      END
		      IF nY < self.sldValueCount.Value THEN cSeries = cSeries + ","
		    NEXT
		    
		    cCaption = cWords.NthField( "," , nX )
		    CP.addSeries( cCaption, cSeries )
		  NEXT
		  
		  FOR nY = 1 TO self.sldValueCount.Value
		    cLegend = cLegend + cLegendWords.nthField(",", nY)
		    IF nY < self.sldValueCount.Value THEN cLegend = cLegend + ","
		  NEXT
		  
		  CP.title = "Sales"
		  CP.AddLegend( cLegend )
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GenRandomData2()
		  DIM oRandom AS New Random
		  DIM nX, nY, nValue AS Integer
		  // Look up these words!  It's a hoot.
		  DIM cWords AS String = "aeolist, bombilate, causeuse, dompteuse, estrapade, farctate, gargalesis, hadeharia, inaniloquent, jentacular, knismesis, liripip, mytacism, nelipot, onychophagy, petrichor, qualtagh, sabrage, thelemic, ulotrichous, ventripotent, witzelsucht, xerophagy, yclept, zenzizenzizenzic"
		  DIM cLegendWords AS String = "one,two,three,four,five,size,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen,twenty,twentyone,twentytwo,twentythree,twentyfour,twentyfive,twentysix"
		  DIM cSeries AS String = ""
		  DIM cCaption, cLegend AS String = ""
		  CP.resetChart
		  CP.clearValues
		  
		  cSeries = ""
		  FOR nX = 1 TO self.sldValueCount.Value
		    nValue = oRandom.InRange( 1, 1000000 )
		    cSeries = cSeries + Str( nValue )
		    cLegend = cLegend + cLegendWords.NthField( ",", nX )
		    IF nX < self.sldValueCount.Value THEN
		      cSeries = cSeries + ","
		      cLegend = cLegend + ","
		    end if
		  next
		  
		  
		  nX = oRandom.InRange( 1, CountFields( cWords, "," ) )
		  cCaption = cWords.NthField( "," , nX )
		  CP.addSeries( cCaption, cSeries )
		  CP.addLegend( cLegend )
		  CP.title = cCaption
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GenRandomData3()
		  DIM oRandom AS New Random
		  DIM nX, nY, nValue AS Integer
		  // Look up these words!  It's a hoot.
		  DIM cWords AS String = "aeolist, bombilate, causeuse, dompteuse, estrapade, farctate, gargalesis, hadeharia, inaniloquent, jentacular, knismesis, liripip, mytacism, nelipot, onychophagy, petrichor, qualtagh, sabrage, thelemic, ulotrichous, ventripotent, witzelsucht, xerophagy, yclept, zenzizenzizenzic"
		  DIM cLegendWords AS String = "one,two,three,four,five,size,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen,twenty,twentyone,twentytwo,twentythree,twentyfour,twentyfive,twentysix"
		  DIM cSeries AS String = ""
		  DIM cCaption, cLegend AS String = ""
		  CP.resetChart
		  CP.clearValues
		  
		  cSeries = ""
		  FOR nX = 1 TO self.sldValueCount.Value
		    nValue = oRandom.InRange( 10, 20 )
		    IF oRandom.InRange(1,2) = 1 THEN
		      nValue = nValue * -1
		    END IF
		    cSeries = cSeries + Str( nValue )
		    cLegend = cLegend + cLegendWords.NthField( ",", nX )
		    IF nX < self.sldValueCount.Value THEN
		      cSeries = cSeries + ","
		      cLegend = cLegend + ","
		    end if
		  next
		  
		  
		  nX = oRandom.InRange( 1, CountFields( cWords, "," ) )
		  cCaption = cWords.NthField( "," , nX )
		  CP.addSeries( cCaption, cSeries )
		  CP.addLegend( cLegend )
		  CP.title = cCaption
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadColorPalettes()
		  DIM nX AS Integer = 0
		  DIM cValue AS String = ""
		  self.cboPalette.DeleteAllRows
		  
		  FOR nX = 1 TO CP.PaletteList.Count
		    cValue = CP.PaletteList.Value( nX )
		    self.cboPalette.AddRow( cValue )
		  NEXT
		  me.cboPalette.ListIndex = 0
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub showFontInfo()
		  CP.Redraw()
		  
		  lblTitleTextSize.Caption = "titleTextSize: " + CSTR( CP.titleTextSize )
		  lblLabelTextSize.Caption = "labelTextSize: " + CSTR( CP.labelTextSize )
		  
		  lblTitleTextSize.TextSize = Cp.titleTextSize
		  lblLabelTextSize.TextSize = Cp.labelTextSize
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub showLegend()
		  CP.legendShow = self.chkShowLegend.Value
		  CP.Redraw()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub showLegendBorder()
		  CP.legendBorder = self.chkLegendBorder.Value
		  CP.Redraw()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub showLegendValues()
		  DIM cLocation AS String = ""
		  
		  SELECT CASE CP.legendLocation
		  CASE 1
		    cLocation = "1: Right"
		  CASE 2
		    cLocation = "2: Left"
		  CASE 3
		    cLocation = "3: Bottom (TBD)"
		  CASE 4
		    cLocation = "4: Top (TBD)"
		  END
		  self.lblLegendLocation.Caption = "legendLocation: " + cLocation
		  ' self.chkShowLegend.Value = CP.legendShow
		  
		  self.lblLegendNodeSize.Caption = "legendNodeSize: " + CSTR( CP.legendNodeSize )
		  self.lblLegendPadding.Caption = "legendPadding: " + CSTR( CP.legendPadding )
		  self.lblLegendLineSpacing.Caption = "legendLineSpacing: " + CSTR( CP.legendLineSpacing )
		  self.lblLegendTextSize.Caption = "legendTextSize: " + CSTR( CP.legendTextSize )
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub showMargins()
		  CP.ShowMargins = self.chkShowMargins.Value
		  CP.clearChart()
		  CP.Redraw()
		  self.ShowMarginValues()
		  
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub showMarginValues()
		  self.lblMarginTop.Caption = "marginTop: " + CSTR( CP.MarginTop )
		  self.lblMarginLeft.Caption = "marginLeft: " + CSTR( CP.MarginLeft )
		  self.lblMarginBottom.Caption = "marginBottom: " + CSTR( CP.MarginBottom )
		  self.lblMarginRight.Caption = "marginRight: " + CSTR( CP.MarginRight )
		  self.lblChartWidth.Caption = "chartWidth: " + CSTR( CP.chartWidth )
		  self.lblChartHeight.Caption = "chartHeight: " + CSTR( CP.chartHeight )
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		nValueMax As Integer = 100
	#tag EndProperty

	#tag Property, Flags = &h0
		nValueMin As Integer = -100
	#tag EndProperty


#tag EndWindowCode

#tag Events sldSeriesCount
	#tag Event
		Sub ValueChanged()
		  self.GenRandomData()
		  lblSeries.Caption = "Series Count: " + CSTR(me.Value)
		  CP.Redraw()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cboPalette
	#tag Event
		Sub Change()
		  self.CP.nPalette= me.ListIndex + 1
		  self.CP.LoadPalette( )
		  CP.redraw
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events sldNodeSize
	#tag Event
		Sub ValueChanged()
		  CP.NodeSize = me.Value
		  CP.Redraw
		  lblLineChartNodeSize.Caption = "Line Chart :: nodeSize: " + CSTR( me.Value )
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events chkRules
	#tag Event
		Sub Action()
		  CP.Rules = 1
		  IF me.Value THEN
		    CP.Rules = 2
		  END IF
		  CP.Redraw
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events sldShadow
	#tag Event
		Sub ValueChanged()
		  CP.shadowSize = me.Value
		  CP.redraw
		  lblShadow.Caption = "shadowSize: " + CSTR( me.Value )
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events scrSeriesValueLocation
	#tag Event
		Sub ValueChanged()
		  CP.seriesValueLocation = me.Value
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events chkSeriesValues
	#tag Event
		Sub Action()
		  CP.seriesValues = me.Value
		  CP.Redraw()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events chkLabelSeries
	#tag Event
		Sub Action()
		  CP.labelSeries = me.Value
		  CP.Redraw
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events chkLabelShadow
	#tag Event
		Sub Action()
		  CP.labelShadow = me.Value
		  CP.Redraw
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events chkLabelValues
	#tag Event
		Sub Action()
		  CP.labelValues = me.Value
		  CP.Redraw
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events chkShowLegend
	#tag Event
		Sub Action()
		  showLegend()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events spnLabelFontSize
	#tag Event
		Sub Down()
		  CP.labelTextSize = CP.labelTextSize - 1
		  IF CP.labelTextSize < 1 THEN CP.labelTextSize = 1
		  self.showFontInfo()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Up()
		  CP.labelTextSize = CP.labelTextSize + 1
		  
		  self.showFontInfo()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events spnTitleFontSize
	#tag Event
		Sub Down()
		  CP.titleTextSize = CP.titleTextSize - 1
		  IF CP.titleTextSize < 1 THEN CP.titleTextSize = 1
		  self.showFontInfo()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Up()
		  CP.titleTextSize = CP.titleTextSize + 1
		  
		  self.showFontInfo()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events spnMarginTop
	#tag Event
		Sub Down()
		  CP.marginTop = CP.marginTop - 1
		  IF CP.marginTop < 0 THEN CP.marginTop = 0
		  self.showMargins()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Up()
		  CP.marginTop = CP.marginTop + 1
		  IF CP.marginTop > CP.Height - CP.marginBottom THEN CP.marginTop = CP.Height - CP.marginBottom - 1
		  self.showMargins()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events spnMarginLeft
	#tag Event
		Sub Down()
		  CP.marginLeft = CP.marginLeft - 1
		  IF CP.marginLeft < 0 THEN CP.marginLeft = 0
		  self.showMargins()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Up()
		  CP.marginLeft = CP.marginLeft + 1
		  IF CP.marginLeft > CP.Width - CP.marginRight THEN CP.marginLeft = CP.Width - CP.marginRight - 1
		  self.showMargins()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events spnMarginBottom
	#tag Event
		Sub Down()
		  CP.marginBottom = CP.marginBottom - 1
		  IF CP.marginBottom < 0 THEN CP.marginBottom = 0
		  self.showMargins()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Up()
		  CP.marginBottom = CP.marginBottom + 1
		  IF CP.marginBottom > CP.Height - CP.marginTop THEN CP.marginBottom = CP.Height - CP.marginTop - 1
		  self.showMargins()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events spnMarginRight
	#tag Event
		Sub Down()
		  CP.marginRight = CP.marginRight - 1
		  IF CP.marginRight < 0 THEN CP.marginRight = 0
		  self.showMargins()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Up()
		  CP.marginRight = CP.marginRight + 1
		  IF CP.marginRight > CP.Width - CP.marginLeft THEN CP.marginRight = CP.Width - CP.marginLeft - 1
		  self.showMargins()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events chkShowMargins
	#tag Event
		Sub Action()
		  self.showMargins()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ScrSeriesLabelLocation
	#tag Event
		Sub ValueChanged()
		  DIM cSeriesLabelLoc AS String = ""
		  CP.labelSeriesLocation = me.Value
		  self.lblSeriesLabelLocation.Caption = "Series Label Loc: " + STR(me.Value)
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events sldValueCount
	#tag Event
		Sub ValueChanged()
		  self.GenRandomData()
		  lblValues.Caption = "Value Count: " + CSTR(me.Value)
		  CP.Redraw()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events spnLegendNodeSize
	#tag Event
		Sub Down()
		  CP.legendNodeSize = CP.legendNodeSize - 1
		  IF CP.legendNodeSize < 2 THEN CP.legendNodeSize = 2
		  
		  self.showLegendValues()
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Up()
		  CP.legendNodeSize = CP.legendNodeSize + 1
		  IF CP.legendNodeSize > 20 THEN CP.legendNodeSize = 20
		  
		  self.showLegendValues()
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events spnLegendPadding
	#tag Event
		Sub Down()
		  CP.legendPadding = CP.legendPadding - 1
		  IF CP.legendPadding < 1 THEN CP.legendPadding = 1
		  
		  self.showLegendValues()
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Up()
		  CP.legendPadding = CP.legendPadding + 1
		  IF CP.legendPadding > 20 THEN CP.legendPadding = 20
		  
		  self.showLegendValues()
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events spnLegendLineSpacing
	#tag Event
		Sub Down()
		  CP.legendLineSpacing = CP.legendLineSpacing - 1
		  IF CP.legendLineSpacing < 1 THEN CP.legendLineSpacing = 1
		  
		  self.showLegendValues()
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Up()
		  CP.legendLineSpacing = CP.legendLineSpacing + 1
		  IF CP.legendLineSpacing > 20 THEN CP.legendLineSpacing = 20
		  
		  self.showLegendValues()
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events spnLegendTextSize
	#tag Event
		Sub Down()
		  CP.legendTextSize = CP.legendTextSize - 1
		  IF CP.legendTextSize < 1 THEN CP.legendTextSize = 1
		  
		  self.showLegendValues()
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Up()
		  CP.legendTextSize = CP.legendTextSize + 1
		  IF CP.legendTextSize > 20 THEN CP.legendTextSize = 20
		  
		  self.showLegendValues()
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events spnLegendLocation
	#tag Event
		Sub Down()
		  CP.legendLocation = CP.legendLocation - 1
		  IF CP.legendLocation < 1 THEN CP.legendLocation = 1
		  
		  self.showLegendValues()
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Up()
		  CP.legendLocation = CP.legendLocation + 1
		  IF CP.legendLocation > 4 THEN CP.legendLocation = 4
		  
		  self.showLegendValues()
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events chkLegendBorder
	#tag Event
		Sub Action()
		  showLegendBorder()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events sldLabelValueLocation
	#tag Event
		Sub ValueChanged()
		  DIM cLocationDescription AS String = ""
		  CP.labelValueLocation = me.Value
		  SELECT CASE me.Value
		  CASE 1
		    cLocationDescription = "1 (Top)"
		  CASE 2
		    cLocationDescription = "2 (Bottom)"
		  CASE 3
		    cLocationDescription = "3 (Center Vertically)"
		  ELSE
		    Break
		  END SELECT
		  
		  self.lblLabelValueLocation.Caption = "labelValueLocation: " + cLocationDescription
		  CP.Redraw()
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="nValueMax"
		Group="Behavior"
		InitialValue="100"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="nValueMin"
		Group="Behavior"
		InitialValue="-100"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior

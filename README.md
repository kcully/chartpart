# README #

ChartPart is a Custom Control for [xojo.com](http://Xojo.com). It makes it very easy to build charts and graphs within Xojo programs.

### What is this repository for? ###

* Get the latest version of ChartPart version 5.4
* Version 5.4
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Download the free Xojo development IDE
* Download this repository
* Run the Sample Application provided called chartpart.xojo_project
* The sample application helps you learn what is possible and what the properties do to change the look of the chart output
* When you are ready, copy the ChartPart class into your own applications and take advantage of its capabilities

### Contribution guidelines ###

* Please provide feedback to help us grow the abilities of this open source control for Xojo developers.

### Who do I talk to? ###

* Contact developer through CULLY Technologies [Contact Us](http://cullytechnologies.com/ctcontact.php) page